# -*- coding: utf-8 -*-
import Tkinter
import tkFileDialog
import os
import glob


# Расширение файла
def get_extension(path):
    file_name = path.rfind('.')
    if file_name > 0:
        file_name = path[file_name:]
        return file_name


# Только путь к файлу
def get_path_file(path):
    index_start_1 = path.rfind('/')
    index_start_2 = path.rfind('\\')
    if index_start_1 > index_start_2:
        result = path[0:index_start_1]
    else:
        result = path[0:index_start_2]
    return result


# Только имя файла без расширения
def get_name_file(path):
    index_start_1 = path.rfind('/')
    index_start_2 = path.rfind('\\')
    index_finish = path.rfind('.')
    if index_start_1 > index_start_2:
        name = path[index_start_1 + 1:index_finish]
    else:
        name = path[index_start_2 + 1:index_finish]
    return name


class NButton(Tkinter.Button):
    def __init__(self, *args, **kw):
        Tkinter.Button.__init__(self, *args,
                                width=15,
                                height=2,
                                foreground='#BBBBBB',
                                background='#494E50',
                                relief='flat',
                                activebackground='#4E95BE',
                                activeforeground='#BBBBBB',
                                borderwidth=0,
                                font='14',
                                **kw)


class NTk(Tkinter.Tk):
    def __init__(self):
        Tkinter.Tk.__init__(self)
        # self.overrideredirect(True)
        self.config(bg='#313335')


class NLabel(Tkinter.Label):
    def __init__(self, *args, **kw):
        Tkinter.Label.__init__(self, *args, bg='#313335', fg='#BBBBBB', **kw)


class Main(NTk):
    def __init__(self):
        NTk.__init__(self)
        self.title('FinderPy')
        self.resizable(False, False)
        self.first_path = ''
        self.second_path = ''
        self.types = ['*.png', '*.jpg', '*.jpeg', '*.bmp']
        self.lists = []
        self.conf()

        NButton(self, text="Откуда", command=self.in_path).grid(row=1, column=0)
        NButton(self, text="Куда", command=self.out_path).grid(row=1, column=1)
        NButton(self, text="Начать", command=self.start).grid(row=1, column=2)
        self.label = NLabel(self, text='')
        self.label.grid(row=2, column=0, columnspan=3, sticky='w')
        self.label['text'] = 'Готово'
        self.iconbitmap('ico.ico')

    def conf(self):
        config = open('config.cfg').read()
        if len(config) == 0:
            with open('config.cfg', 'w') as config:
                for item in self.types:
                    config.write(item + '\n')
        else:
            config = config.split('\n')
            self.types = []
            for item in config:
                if len(item) > 0:
                    self.types.append(item)
        return

    def in_path(self):
        self.first_path = tkFileDialog.askdirectory()

    def out_path(self):
        self.second_path = tkFileDialog.askdirectory()

    # Начать обработку
    def start(self):
        if len(self.first_path) == 0 or len(self.second_path) == 0:
            return
        self.detect()
        if len(self.lists) == 0:
            return
        self.rename()

    # Найти картинки и записать пути в список
    def detect(self):
        if len(self.first_path) == 0:
            return
        self.lists = []
        self.label['text'] = 'Поиск...'
        count = 0
        for item in self.types:
            interim_full = []
            depth = '/'
            while 1:
                if len(glob.glob(self.first_path + depth + '*')) > 0:
                    interim_list = glob.glob(self.first_path + depth + item)
                    depth += '*/'
                    if len(interim_list) > 0:
                        interim_full.extend(interim_list)
                else:
                    if len(interim_full) > 0:
                        self.lists.append(interim_full)
                    break
            count += len(interim_full)
            print 'Search: ', item, len(interim_full), ' шт.'
        self.label['text'] = 'Найдено: ', count, ' шт.'

    # Перенести
    def rename(self):
        if len(self.lists) == 0 or len(self.second_path) == 0:
            return
        for item in self.lists:
            self.label['text'] = item
            name = get_name_file(item)
            ext = get_extension(item)
            new_name = self.second_path + '\\' + name + ext
            index = 0
            while True:
                if item != new_name:
                    os.rename(item, new_name)
                    break
                new_name = self.second_path + '\\' + name + '_' + index + ext
                index += 1
        self.label['text'] = 'Готово'
        return


Main().mainloop()
