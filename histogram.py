# -*- coding: utf-8 -*-
import Tkinter
import glob
import os
import tkFileDialog
import cv2
import numpy as np


def calc_histogram(image):
    rgb = []
    for i, col in enumerate(('b', 'g', 'r')):
        histr = cv2.calcHist([image], [i], None, [256], [0, 256])
        rgb.append(histr)
    return rgb_general(rgb)


# Вычисление максимальной точки
def maximum(b, g, r, result):
    val = max(b, g, r)
    if val == b:
        result['blue'] += 1
    if val == g:
        result['green'] += 1
    if val == r:
        result['red'] += 1
    return result


# Вычисление среднего значения
def rgb_general(hist):
    blue = hist[0]
    green = hist[1]
    red = hist[2]
    result = {'red': 0, 'green': 0, 'blue': 0}
    for i in xrange(0, 255):
        result = maximum(blue[i][0], green[i][0], red[i][0], result)
    name = 'r' + str(result['red']) + '_g' + str(result['green']) + '_b' + str(result['blue'])
    return name


# Открыть картинку
def open_image(image):
    try:
        img = cv2.imread(image)
    except UnicodeEncodeError:
        try:
            img = cv2.imread(image.encode('cp1251'))
        except:
            print 'The cache is overflowing in ', image
            return
    if img is None:
        print 'The image is not parsed: ', image
        return
    return img


# Расширение файла
def get_extension(path):
    file_name = path.rfind('.')
    if file_name > 0:
        file_name = path[file_name:]
        return file_name


# Только путь к файлу
def get_path_file(path):
    index_start_1 = path.rfind('/')
    index_start_2 = path.rfind('\\')
    if index_start_1 > index_start_2:
        result = path[0:index_start_1]
    else:
        result = path[0:index_start_2]
    return result


# Только имя файла без расширения
def get_name_file(path):
    index_start_1 = path.rfind('/')
    index_start_2 = path.rfind('\\')
    index_finish = path.rfind('.')
    if index_start_1 > index_start_2:
        name = path[index_start_1 + 1:index_finish]
    else:
        name = path[index_start_2 + 1:index_finish]
    return name


class NButton(Tkinter.Button):
    def __init__(self, *args, **kw):
        Tkinter.Button.__init__(self, *args,
                                width=15,
                                height=2,
                                foreground='#BBBBBB',
                                background='#494E50',
                                relief='flat',
                                activebackground='#4E95BE',
                                activeforeground='#BBBBBB',
                                borderwidth=0,
                                font='14',
                                **kw)


class NTk(Tkinter.Tk):
    def __init__(self):
        Tkinter.Tk.__init__(self)
        # self.overrideredirect(True)
        self.config(bg='#313335')


class NLabel(Tkinter.Label):
    def __init__(self, *args, **kw):
        Tkinter.Label.__init__(self, *args, bg='#313335', fg='#BBBBBB', **kw)


class NScrollbar(Tkinter.Scrollbar):
    def __init__(self, *args, **kw):
        Tkinter.Scrollbar.__init__(self, *args, bg='#494E50', bd='0', **kw)


class NListbox(Tkinter.Listbox):
    def __init__(self, *args, **kw):
        Tkinter.Listbox.__init__(self, *args,
                                 height=8,
                                 width=40,
                                 selectmode='SINGLE',
                                 activestyle='none',
                                 background='#3C3F41',
                                 foreground='#BBBBBB',
                                 selectbackground='#0D293E',
                                 selectforeground='#BBBBBB',
                                 highlightcolor='#282828',
                                 highlightbackground='#282828',
                                 relief='flat',
                                 **kw)


class Main(NTk):
    def __init__(self):
        NTk.__init__(self)
        self.title('Histogram')
        self.resizable(False, False)
        self.path = ''
        self.types = ['*.png', '*.jpg', '*.jpeg', '*.bmp']
        self.lists = []  # Список путей изображений
        self.classes = {}  # Данные разбиты по классам
        self.duplicate = {}  # Список найденных дупликатов
        self.suspense = {}  # Словарь неопределённых массивов с дупликатами
        self.exception = []  # Список файлов исключений
        self.duplicate_path = 'duplicate'
        self.exception_path = 'exceptions'
        self.list_paths = None
        self.config_path = []
        self.conf_read()
        NButton(self, text="Выбор директории", command=self.change_path).grid(row=1, column=0)
        NButton(self, text="Начать", command=self.start).grid(row=1, column=1)
        NButton(self, text="Перенести", command=self.end).grid(row=1, column=2)
        self.label = NLabel(self, text='')
        self.label.grid(row=2, column=0, columnspan=3, sticky='w')
        self.label['text'] = 'Готово'
        self.iconbitmap('icon.ico')

    def conf_read(self):
        config = open('histogram.cfg').read()
        self.config_path = []
        if len(config) > 0:
            config = config.split('\n')
            for item in config:
                if len(item) > 0:
                    self.config_path.append(item)
        return

    def conf_write(self):
        config = open('histogram.cfg', 'w')
        for path in self.config_path:
            if len(path) > 0:
                config.write(path + '\n')
        config.close()
        return

    # Модальное с выбранными путями
    def open_path(self):
        self.list_paths = Tkinter.Toplevel()
        self.list_paths.title("Путь")
        self.list_paths.resizable(False, False)
        self.list_paths.config(bg='#313335')
        listbox = NListbox(self.list_paths)
        for path in self.config_path:
            if len(path) > 0:
                listbox.insert('end', path)
        listbox.grid(row=1, column=0, rowspan=3)
        listbox.bind("<<ListboxSelect>>", self.get_path)

        '''
        scrollbar = NScrollbar(listbox, orient='vertical')
        scrollbar.pack(side='right')
        scrollbar['command'] = listbox.yview
        listbox['yscrollcommand'] = scrollbar.set


        xscrollbar = NScrollbar(listbox, orient='horizontal')
        xscrollbar.pack(side='bottom', fill='both')
        xscrollbar['command'] = listbox.xview
        listbox['xscrollcommand'] = xscrollbar.set
        '''

        NButton(self.list_paths, text="Новый", command=self.modal_directory).grid(row=1, column=1)
        NButton(self.list_paths, text="Выбрать", command=self.detect).grid(row=2, column=1)
        NButton(self.list_paths, text="Отмена", command=self.list_paths.destroy).grid(row=3, column=1)
        self.list_paths.focus_set()
        self.list_paths.grab_set()
        self.list_paths.wait_window()

    # Обработка события при нажатии по пути в списке
    def get_path(self, event):
        l = event.widget
        sel = l.curselection()
        if len(sel) == 1:
            s = l.get(sel[0])
            if s[0] == '-':
                l.selection_clear(sel[0])
            else:
                print s
                self.path = s
        return

    # Определить: какое из двух модальных окон открыть
    def change_path(self):
        if len(self.config_path) > 0:
            self.open_path()
        else:
            self.modal_directory()

    # Модальное окно выбора директории
    def modal_directory(self):
        self.path = tkFileDialog.askdirectory()
        added_path = self.path.encode('utf8')
        is_added = False
        if len(added_path) == 0:
            return
        for item in self.config_path:
            if added_path == item:
                is_added = True
        if is_added:
            self.config_path.append(added_path)
            self.conf_write()
        self.list_paths.destroy()
        self.detect()

    # Найти картинки и записать пути в список
    def detect(self):
        if len(self.path) == 0:
            return
        self.lists = []
        for item in self.types:
            interim_list = []
            interim_full = []
            depth = '/'
            while 1:
                if len(glob.glob(self.path + depth + '*')) > 0:
                    interim_list = glob.glob(self.path + depth + item)
                    depth += '*/'
                    if len(interim_list) > 0:
                        interim_full.extend(interim_list)
                else:
                    if len(interim_full) > 0:
                        self.lists.append(interim_full)
                    break

            print 'Find: ', item, len(interim_full)
        self.del_exceptions()
        self.list_paths.destroy()

    # Удалить файлы из папки исключения
    def del_exceptions(self):
        tmp = self.path + '\\' + self.duplicate_path
        exclude = []
        include = []
        for cls in self.lists:
            for image in cls:
                if get_path_file(image) == tmp:
                    exclude.append(image)
                else:
                    include.append(image)
        print 'Files exclude: ', len(exclude)
        print 'Files include:', len(include)
        self.lists = include
        return

    # Объединение картинок по категориям
    def division(self, image, path):
        shape = image.shape
        shape = str(shape[0]) + '_' + str(shape[1])
        histogram = calc_histogram(image)
        if not (shape in self.classes):
            self.classes[shape] = {}
        if histogram in self.classes[shape]:
            self.classes[shape][histogram].append(path)
        else:
            self.classes[shape][histogram] = []
            self.classes[shape][histogram].append(path)
        return

    # Вывести результат
    def print_result(self):
        self.suspense = {}
        for shape, val in self.classes.items():
            # print shape, ' {'
            for histogram, images in val.items():
                if len(images) > 1:
                    print shape, histogram, len(images)
                    self.suspense[histogram] = images
                # print '\t', histogram, ' : ', len(images)
            # print '}'
        return

    # Начать обработку
    def start(self):
        if len(self.path) == 0 or len(self.lists) == 0:
            return
        index = 0
        for path in self.lists:
            image = open_image(path)
            if image is not None:
                self.division(image, path)
            index += 1
            print index, ' from ', len(self.lists)
            self.label['text'] = str(index) + ' from ' + str(len(self.lists))
        self.label['text'] = 'Готово'
        self.print_result()
        self.check_duplicate()

    # Перенести файлы
    def end(self):
        self.move_duplicate()
        print 'Ready'
        self.label['text'] = 'Готово'

    # Проверка результата на дупликаты
    def check_duplicate(self):
        count = 0
        index = 0
        for key, val in self.suspense.items():
            count += self.rewind(key, val)
            index += 1
            print index, ' from ', len(self.suspense)
            self.label['text'] = str(index) + ' from ' + str(len(self.suspense))
        print 'Count duplicate: ', count
        self.label['text'] = 'Число дубликатов: ' + str(count)
        return

    # Перемотка дупликатов для подтверждения
    def rewind(self, name, paths):
        count = 0
        for path in paths:
            for item in paths:
                if path != item:
                    first = open_image(path)
                    second = open_image(item)
                    if np.array_equal(first, second):
                        count += self.append_duplicate(name, [path, item])
        # print name, count
        return count

    # Добавить и посчитать
    def append_duplicate(self, key, items):
        count = 0
        if not (key in self.duplicate):
            self.duplicate[key] = []
        for item in items:
            if self.duplicate[key].count(item) == 0:
                self.duplicate[key].append(item)
                count += 1
        return count

    # Пройтись по списку дупликатов
    def move_duplicate(self):
        if len(self.duplicate) > 0:
            for name, paths in self.duplicate.items():
                for path in paths:
                    self.rename_duplicate(name, path)
        return

    # Перенести дупликаты вместе с оригиналами в папку исключения
    def rename_duplicate(self, name, path):
            new_path = self.path + '\\' + self.duplicate_path
            if not os.path.isdir(new_path):
                os.mkdir(new_path)
            # name = get_name_file(old_path)
            ext = get_extension(path)
            new_name = new_path + '\\' + name + ext
            index = 0
            while True:
                if path != new_name and not os.path.exists(new_name):
                    os.rename(path, new_name)
                    break
                new_name = new_path + '\\' + name + '_' + str(index) + ext
                index += 1
            return


Main().mainloop()
